# OAuth SAP Gateway with Keycloak

Information on how to achieve OAuth 2.0 authentication with SAP Gateway as SP and Keycloak as IdP

The repository contains additional information for my blog series on how to configure OAuth 2.0 for an SAP Gateway Service and Keycloak.

## SAML

The SAML metadata files for SP and IdP are included for reference. Please remember that these files are useless, as both were created by the SP and IdP. Your own IdP and SP metadata files will be different, and importing my files won't work.

* npl_001_sp_metadata.xml: Gateway SP metadata
* keycloak_idp_saml_metadata: Keycloak IdP metadata

## Postman

The postman file contains a set of requests to get an OAuth Access Token from SAP Gateway. To get a better understanding of the requests, I provide a small documentation. 

The requests depend on some variables.  Make sure to set them according to your infrastructure.

### Keycloak

Set the values according to your Keycloak server. The default settings should work when running a default installation of Keycloak

* keycloak_protocol
* keycloak_server
* keycloak_port
* keycloak_realm
* SAMLRequest See below for more information on the SAMLRequest send to Keycloak

### User

* user_name Your username in Keycloak
* user_password Password for the user in Keycloak

### Gateway

* grant_type Grant type for SAML 2.0 Bearer assertion flow
* client_id The OAuth client you configured in SAP Gateway for OAuth
* scope The scope of the OAuth service. Must be assigned to the OAuth client
* gateway_protocol
* gateway_server
* gateway_port
* gateway_odata_path
* gateway_service OData service configured for OAuth
* gateway_service_params

```javascript
pm.globals.set("keycloak_protocol", "http");
pm.globals.set("keycloak_server", "localhost");
pm.globals.set("keycloak_port", "8080");
pm.globals.set("keycloak_realm", "master");
pm.globals.set("user_name", "yourusername");
pm.globals.set("user_password", "insertyourpassword");
pm.globals.set("grant_type", "urn:ietf:params:oauth:grant-type:saml2-bearer");
pm.globals.set("client_id", "OIDCLIENT");
pm.globals.set("scope", "ZDEMO_CDS_SALESORDERITEM_CDS_0001");
pm.globals.set("gateway_protocol", "https");
pm.globals.set("gateway_server", "vhcalnplci");
pm.globals.set("gateway_port", "44300");
pm.globals.set("gateway_odata_path", "sap/opu/odata/sap/");
pm.globals.set("gateway_service", "ZDEMO_CDS_SALESORDERITEM_CDS");
pm.globals.set("gateway_service_params", "?$format=json");
pm.globals.set("SAMLRequest", SAMLRequest);
```

1. Keycloak logout

Logout of Keycloak. Only works when you were logged in previsously. Makes it easier to run the REST requests several times in a row without having to check if Keycloak is using an old session to authenticate you.

2. Login Keycloak IdP SAML 2.0 Request

Send a SAMLRequest from a SAML Client to Keycloak. Triggers SAML authentication flow at Keycloak. Keycloak returns login form, asking user to authenticate.

3. Login to Keycloak

Send login information to Keycloak (UIDPW). After authentication, Keycloak returns SAMLResponse with SAML Assertion information. This information is needed to log in to the SAP Gateway Token provider service.

4. Get SAP Gateway Access Token

Sends SAML 2.0 Bearer data to SAP Gateway Access token provider service. Gets back a valid access token from SAP Gateway. This token can be used to authenticate against the OAuth 2.0 OData service.

5. Call SAP Gateway OData Service OAuth

## SAMLRequest

The SAMLRequest used is:

```XML
fZHNasMwEIRfxejuSkqaxFliQ2goGNIQmtJDb4q8IQJZcrVyf96%2BskNLemivw858M9KKVGs7WPfx7B7xtUeKWb0p2UEUQkwWi0UubhfzXGKj8kJqkS%2BPONPT4xxFU7DsGQMZ70o2uREsq4l6rB1F5WKShFzmUuZy%2BiQlCAGz4oVlm0QwTsXRdY6xA86t18qePUUoEparVIYHVLYl3iqKGHgXfPTaWz7UZdm9DxrHziU7KUs4sPeKyLzhj%2FLRWkcw7itZHxx4RYbAqRYJoobD%2BmELqTd8h7NqNVzDOCNc%2Bf%2B3JyyGYQ%2BrdvutEHLFr2IumR3skq%2Fe7L01%2BnPo36r4d6y8kaNimvw0nkLvqENtTgYblq2t9e936YViWhtDj4xXF%2Bjvn6y%2BAA%3D%3D&RelayState=oucqyqqsxxxoquxworedaoyrcqozbevctbweqdy&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23rsa-sha1&Signature=LPIE3a5TWxycz5PUToEx3RjqcCUAvqwdS0oVKmZZmzrYwADAgvS%2F5vSx3NujwF5q7O6BzU9mAl2tOaGjgCHFqcuxeApeDNUlwT3Y352%2BvKZuzFhMxXcV2njB4srwHBlOhCyr1bri9ZPyadF08W0bGyoYBZyghDhHO0oWIiIQN1ab7Z6Z00JVpGtQqaWHJGUogdy%2FCKTBl8bu5AUTd%2BXY%2F9MGoSyZvcjXC8n6hJ6Ahpwg9HFGtzigKn7M1zs5KP9ymx7jOKa36UMehHw%2FB%2F%2BlTdavkzMWcAyaL6O%2BsySCYsAi5XG2KtOPipv0E0bbDu32fl4Cge4f4Px2P4MLaee4Iw%3D%3D
```

Decoded:

To decode, just use on of the freely available SAMLRequest decoders on the internet.

```XML
<samlp:AuthnRequest ID="S08002777-0476-1eda-81c0-9be5c3b6e0d8" Version="2.0" IssueInstant="2019-11-13T11:00:58Z" Destination="http://localhost:8080/auth/realms/master/protocol/saml" ForceAuthn="false" IsPassive="false" xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol"><saml:Issuer xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">NPL001</saml:Issuer><samlp:NameIDPolicy Format="urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified" AllowCreate="true"/></samlp:AuthnRequest>
```